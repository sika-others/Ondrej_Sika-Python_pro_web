Flask
-----
Pokud nechceme jen statické stráky bez možnosti interakce s uživatelem, flask-server nám nebude stačit. Proto si musíme napsat flask aplikaci sami. Není to nic obtížného.

### Instalace flasku
Pokud jsme již instalovaly flask-server, měly bychom již flask mít (stáhl se jako závislost u flask-serveru), ale pokud ho nemáte, naistalujte ho příkazem:

    root@ntb:/home/sika# pip install flask

### První aplikace
Vytvoříme si soubor wsgi.py do kterého bude psát aplikaci. Je to hlavní přístupoví bod pro wsgi server. Takto může vypadat vlastní jednoduchá aplikace na flasku.

    from flask import Flask
    app = Flask(__name__)

    @app.route("/")
    def hello():
        return "Hello World!"

    if __name__ == "__main__":
        app.run()

Spustíme jí jako flask-server aplikaci

    sika@ntb:/home/sika/flask$ python wsgi.py
     * Running on http://127.0.0.1:5000/

Teď si rozebereme aplikaci po částech. Flask je dobrý v tom, že je velmi přehledný a jednoudý, vhodný pro malé i velké aplikace.

#### Inicializace flask aplikace
    app = Flask(__name__)

#### Pohled
    @app.route("/")
    def hello():
        return "Hello World!"

Pohled ve flask aplikaci je funkce, která vrací html, které se zobrazí uživateli. Pokud chceme, aby na náš pohled vedla nějaká url, nadefinujeme jí dekorátorem @app.route, kde v parametru je url cesta.

#### Spouštění aplikace
    if __name__ == "__main__":
        app.run()

Pokud chceme aplikaci spouštět, spustíme funkci app.run. Pokud ovšem chceme naši wsgi aplikaci serverovat pomocí wsgi serveru, nemúžeme spouštet funkci app.run, jelikož by přerušila chod programu. Pokud chceme rozlišovat zda soubor spouštíme a nebo jej importujeme, použijeme if `__name__ == "__main__"`.

### Pohledy a url
Ve flasku se můžou definovat pohledy s url na jednom místě. Je to nejjednodušší způsob. Pohled s fixní url se definuje takto:

    app.route("/about-me/")
    def view():
        return "Hi, I'm Ondrej"

Pokud cheme mít v url nějaké parametry vložíme je jako <var> a nebo <int:var> do parametru decoratoru app.route. První proměnná je typu str a druhá int. jiné typy flask nepodporuje. Proměnné musíme uvést jako parametry funkce pohledu.

    app.route("/about-<name>/")
    def view(name):
        return "Hi, I'm %s" % name

Při zadání url /about-Ondrej/ stránka vypíše Hi, I'm Ondrej, při zadání url /about-John/ vypíše Hi, I'm John.

### GET a POST proměnné
Další důležitou částí dynamičnosti webové aplikace je schopnost pracovat zpracovávat GET a POST proměnné. Ve flasku je pojmenování trochu odlišné než u ostatních webových frameworků, ale na funkčnosti to nic nemění.

#### GET
Proměnné typu GET se nacházejí v objektu requestu flask.request.args. Přistupujeme k nim jako ke slovníku.

    app.route("/about/")
    def view():
        name = flask.request.args.get("name", "without name")
        return "Hi, I'm %s" % name

Pokud přistoupíme na url /about/ bez parametru name, stránka vypíše Hi, I'm without name. Pokud však zadáme parametr name /about/?name=Ondrej, stránka vypíše Hi, I'm Ondrej.

#### POST
Přístup k post datům je obdobný, je to také slovník v flask.request.form.

### Upload souborů
Nahrané soubory se ve flasku zpracovávají velice jednoduše. Přistupuje se k nim pře flask.request.files jako k slovníku dočasných souborů. Stačí když soubor uložíte metodou save.

    @app.route('/upload')
    def upload_file():
        f = request.files['the_file']
        f.save('/var/www/uploads/uploaded_file.txt')

Pokud chcete ukládat soubor s jeho původním jménem použijte funkci, která zajistí aby se soubor nenahrál do jiného adresáře a odstraní všechny nepovolené znaky.

    from werkzeug import secure_filename

    @app.route('/upload')
    def upload_file():
        f = request.files['the_file']
        f.save('/var/www/uploads/' + secure_filename(f.filename))
