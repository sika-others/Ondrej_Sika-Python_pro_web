Šablonovycí jazyk Jinja2
------------------------
Jinja2 je templatovací jazyk od stejných vývojářů, jako Flak a používá ho i největší webový Python framework Django. Úplnou dokumentaci v angličtině najdete na adrese http://jinja.pocoo.org/docs/.

Hlavní výhodou tohoto jazyka je především jednoduchost použití, která vychází z pythonu, jako který se Jinja2 chová. Zde si popíšeme základní prvky a způsoby použití tohoto templatovacího jazyku na statické stránky poskytované flask-servrem.

### Syntaxe
Jinja2 má velmi jednouchou syntaxi. Pokud chcete provést nějaký příkaz, použijet \{\% ... \%\}. Například pro vložení menu

    {% include "menu.html" %}

Pokud chcete vypsat proměnnou nebo pole nebo slovník použijte \{\{ ... \}\}. Například

    <title>{{title}}</title>

### Deklarace promenné, pole a slovníku
Pokud si vytvořime proměnnou title a zapíšeme její obsah do tagu title, nemusíme už ve všech stránkac opakovat title tag a nebo si ušetříme jeden block (viz. dále).

Proto si vytvoříme proměnnou title zápisem:

    {% set title = "My title" %}

Pokud potřebujeme vytvořit pole (které následně můžeme procházet cyklem for) stačí:

    {% set day = ["mo", "tu", "we"] %}

Je to stejné jako v klasickém Pythonu, obdodně deklarujeme i slovník (dict):

    {% set date = {"day": "1", "month": 1, "year": 1993} %}

### Vkládání
Představte si tento templatovací jazyk jako PHP (které k tomu bylo kdysi určeno). Jistě všichni znáte funkci include. Tato funkce funguje i zde. Zde není co vysvětlovat, vloží jednu šablonu do druhé.


    <ul class="menu">
        {% include "menu.html" %}
    </ul>

V případě vkládání menu bych nepoužil include, protože bychom musely vkládat ješte patičku a meta tagy a už by v souboru bylo hodne includu a to není dobře. Pokud bychom chtěli přidat ješte třeba postraní menu, už by to byl problém, protože bychom museli přidávat do každého souboru další include což je značne nepraktické.

### Rozšiřování
To je funkce která je velmi užiteční a PHP jí postrádá. Jde o to, že si nadefinujete základní šablonu s bloky které pak můžete vyplnit libobolným obsahem v jiných souborech.

#### base.html
    <ul class="menu">
        <li><a href="...."></a></li>
    </ul>
    <div id="content">
        {% block content %}No content{% endblock %}
    </div>
    <div id="footer">
        {% block footer %}No footer{% endblock %}
    </div>

a soubor který rozšiřuje base template, můžeme ho nazvat třeba home.html

#### home.html
    {% extends "base.html" %}
    {% block content %}
        My content
    {% endblock %}

Při renderování souboru home.html se načte soubor base.html a doplní se bloky které obsahuje soubor home.html. Bloky které neobsahuje se vyrenderují stejně jako v původním souboru. Naše stránka bude vypadat takto:

    <ul class="menu">
        <li><a href="...."></a></li>
    </ul>
    <div id="content">
        My content
    </div>
    <div id="footer">
        No footer
    </div>

### Podmínka if
Podmínky jsou ve všech jazycích přibližne stejné, tak nemá cenu je nějak rozpitvávat. Funkčnost tohoto if je stejná jako v Pythonu.

    {% set day = "mo" %}
    {% if day in ["sa", "su"] %}
        It's weekend.
    {% elif day == "fr" %}
        Weekend comming soon!
    {%else %}
        Another work day
    {%endif%}

### For cyklus
For cyklus je zde stejný jako v Pythonu. Cyklus prochízí pole a přižazuej hodnotu pole na i-té pozici do určité proměnné. Příklad:

    {% set days = ["mo", "tu", "we", "th"] %}
    <ul>
    {% for day in days %}
        <li>{{day}}</li>
    {% endfor %}
    </ul>

vypíše

    {% set days = ["mo", "tu", "we", "th"] %}
    <ul>
        <li>mo</li>
        <li>tu</li>
        <li>we</li>
        <li>th</li>
    </ul>


