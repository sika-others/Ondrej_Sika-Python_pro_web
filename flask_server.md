Flask server
------------
Pokud chcete vyvíjet web v PHP, potřebujete lokálně spustit webový server Apache a PHP, pokud se rozhodnete pro Python, nepotřebujete žádný webový server. Python ho má integrovaný v sobě.
Pokud nemáte z Pythonem moc skušeností a nebo chcete opravdu jednoduché webové stránky, je zde vhodná knihovna Flask a Flask-server který nastaví flask do jednoduché podoby pro rychlé použití.
Když by jste vyvíjeli web v PHP je celkem nemožné, jednoduše dělat pěkné url (http://ondrejsika.com/about-me), nejspíše by jste se museli spokojit s (http://ondrejsika.com/about-me.php).

### Instalace
Nejdříve musíme nainstalovat flask-server. Ideální instalace je přez pip (python install package) příkazem

    root@ntb:/home/sika# pip install flask-server 

pokud nemáte naintalovaný pip, nainstalujte ho z systémových repozitářů

    root@ntb:/home/sika# apt-get install pip

### Initializace
Pokud máme nainstalovaný flask-server, můžeme vygenerovat projekt.

    sika@ntb:/home/sika$ flask-server init web

V adresáři web se vytvořil soubor wsgi.py, který funguje jako wsgi aplikace i jako server. Dále s vytvořily složky template a static. Ve složce template se nachází soubory šablon v html a ve složce static jsou statické soubory.

    sika@ntb:/home/sika$ find web
    web
    web/wsgi.py
    web/templates
    web/templates/index.html
    web/static

Server spustíme příkazem

    sika@ntb:/home/sika$ cd web
    sika@ntb:/home/sika/web$ python wsgi.py
     * Running on http://127.0.0.1:5000/

a spustí se na adrese 127.0.0.1 (localhost) a portu 5000. Zobrazí se vám soubor index.html v složce templates.

### Struktura url
Url struktura je závislá na souborech šablon se nacházejí ve složce templates. Flask server projde tuto složku i se všemi jejími podadresáři a pro každý *.html soubor vytvoří patřičnou url podle cesty. Ovšem bez nehezké přípony .html. Pokud máme ve složce template soubor about-me.html, můžeme k němu přistupovat přez url /about-me. Pokud máme ve složce templates složku about a v ní coubory company.html a me.html můžeme k nim přistupovat přes url /about/company a /about/me. Pokud chceme vytvořit index složky, tak jako pro apache vytvoříme index.html. Pro index složky about, url /about, vytvoříme ve složce teplates/about soubor index.html nebo home.html. oba názvy jsou si rovny.

### 404 not found
Pokud se budete pokoušet přistoupit na url, která nemá reprezentaci v žádném souboru, vrátí se vám chybobvá hláška 404 not found. Pokud si chcete definovat tuto stránku, vytvořte si soubor 404.html v adresáři templates. Ten se zobrazí vždy když bude požadována neplatná URL.


