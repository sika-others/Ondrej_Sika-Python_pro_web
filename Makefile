all:
	rm Ondrej_Sika-Python_pro_web.md -f

	cat main.md >> Ondrej_Sika-Python_pro_web.md
	cat flask_server.md >> Ondrej_Sika-Python_pro_web.md
	cat jinja2.md >> Ondrej_Sika-Python_pro_web.md
	cat flask.md >> Ondrej_Sika-Python_pro_web.md
	cat deployment.md >> Ondrej_Sika-Python_pro_web.md

	pandoc Ondrej_Sika-Python_pro_web.md -o Ondrej_Sika-Python_pro_web.html
