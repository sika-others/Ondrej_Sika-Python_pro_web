Deployment pomocí Deploy
------------------------
Pro jednoduché nasazení aplikace použijeme nástroj deploy (http://ondrejsika.com/docs/deploy), který nám práci hodně usnadní.

### Instalace
Nejdříve musíme nainstalovat závislosti jako nginx, gunicorn, supervisor, git a sudo. V debianu je nainstalujeme pomocí 

    root@ntb:/home/sika# apt-get install nginx gunicorn supervisor git sudo

Deploy je napsaný v Pythonu, proto jej můžeme stáhnout přez pip a poté musíme spustit inicializaci

    root@server:/home/sika# pip install deploy
    root@server:/home/sika# deploy-init


### Vytvoření serverové konfigurace
Tímto příkazem vytvoříte server pro aplikaci s názvem `appname` a naslouchá na doménách domain.com, www.domain.com.

    root@server:~# deploy startserver appname "domain.com www.domain.com"

Vytvořil se zároveň repozitář v home dir uživatele git. Vše co do jeho větve master pushnete, spustí se v produkci.

### Stuktura aplikace
V aplikaci musíte dodržovat určitou jednoduchou strukturu.

#### wsgi
V rootu aplikace je nejdůležitějším a nutným souborem wsgi.py kde v proměnné application je wsgi aplikace. Pokud tento soubor bude chybět aplikace vůbec nepoběží.

#### static
Statické soubory jsou v adresáři static a jsou servované s prefixem /static/

#### pip requirements
Pokud používáte Python virtualenv, můžete do něj nainstalovat potřebené balíčky ze souboru requirements.txt v kořenovém adresáři. Virtualenv se vám vytvoří do adresáře env v rootu aplikace.

### Nahrání aplikace na server
Přidáme vzdálený repozitář a pushneme do něj

    git remote add deploy git@domain.com:appname.git
    git push deploy master


### Smazání aplikace
Pokud chceme aplikaci ostranit, spustíme příkaz

    root@server:~#deploy remove "appname"

a on nás požádá o napsání názvu aplikace. Je to ochrana proti neúmyslnému smazání aplikace.